<?php

namespace Drupal\field_group_layout\Plugin\field_group\FieldGroupFormatter;

use Drupal\Component\Utility\Html;
use Drupal\field_group\FieldGroupFormatterBase;

/**
 * Plugin implementation of the 'layout_region' formatter.
 *
 * @FieldGroupFormatter(
 *   id = "layout_region",
 *   label = @Translation("Layout Region"),
 *   description = @Translation("Formatter for layout regions."),
 *   supported_contexts = {
 *     "form",
 *     "view"
 *   }
 * )
 */
class LayoutRegionFormatter extends FieldGroupFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function process(&$element, $processed_object) {

    $element += [
      '#type' => 'fieldset',
      '#title' => $this->getLabel(),
      '#title_display' => 'invisible',
    ];

    if ($this->getSetting('id')) {
      $element['#id'] = Html::getUniqueId($this->getSetting('id'));
    }

    $classes = $this->getClasses();
    if (!empty($classes)) {
      $element += [
        '#attributes' => ['class' => $classes],
      ];
    }

    if ($this->getSetting('required_fields')) {
      $element['#attached']['library'][] = 'field_group/formatter.details';
      $element['#attached']['library'][] = 'field_group/core';
    }

  }

  /**
   * {@inheritdoc}
   */
  public function preRender(&$element, $rendering_object) {
    parent::preRender($element, $rendering_object);
    $this->process($element, $rendering_object);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm() {
    $form = parent::settingsForm();

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {

    $summary = [];

    return $summary;
  }
  
  /**
   * {@inheritdoc}
   */
  public static function defaultContextSettings($context) {
    $defaults = parent::defaultSettings($context);

    return $defaults;
  }

}
