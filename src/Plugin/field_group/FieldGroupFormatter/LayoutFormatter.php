<?php

namespace Drupal\field_group_layout\Plugin\field_group\FieldGroupFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\field_group\FieldGroupFormatterBase;

/**
 * Plugin implementation of the 'default' formatter.
 *
 * @FieldGroupFormatter(
 *   id = "layouts",
 *   label = @Translation("Layouts"),
 *   description = @Translation("Add a layout formatter"),
 *   supported_contexts = {
 *     "form",
 *     "view"
 *   }
 * )
 */
class LayoutFormatter extends FieldGroupFormatterBase {

  /**
   * {@inheritdoc}
   */
  public function process(&$element, $processed_object) {

    // Keep using preRender parent for BC.
    parent::preRender($element, $processed_object);
    if (isset($this->group->field_layout)) {
      $element['#field_layout'] = $this->group->field_layout ?: NULL;
    }

    $element += [
      '#type' => 'field_group_layouts',
      '#effect' => $this->getSetting('effect'),
    ];

    if ($this->getSetting('id')) {
      $element['#id'] = Html::getUniqueId($this->getSetting('id'));
    }

    $classes = $this->getClasses();
    if (!empty($classes)) {
      $element += ['#attributes' => ['class' => $classes]];
    }
    $element['#attached']['library'][] = 'field_group_layout/layouts';
  }

  /**
   * {@inheritdoc}
   */
  public function preRender(&$element, $rendering_object) {
    if (isset($this->group->field_layout)) {
      $element['#field_layout'] = $this->group->field_layout ?: NULL;
    }
    parent::preRender($element, $rendering_object);
    $this->process($element, $rendering_object);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm() {
    $form = parent::settingsForm();
    $layoutManager = \Drupal::service('plugin.manager.core.layout');
    $group = $this->group;
    $display = $this->getDisplaySettings();

    if ($display) {
      $parent_group = isset($group->group_name)
        ? $display->getThirdPartySettings('field_group')[$group->group_name]
        : [];
     }

    if (isset($group->group_name)) {
      $form['field_layout'] = [
        '#type' => 'select',
        '#title' => $this->t('Select a layout'),
        '#description' => $this->t('<b>Changing the Layout will move the fields to the new region provided by the layout.</b>'),
        '#options' => $layoutManager->getLayoutOptions(),
        '#default_value' => !empty($parent_group) ? $parent_group['field_layout'] : '_none',
        '#states' => [
          'visible' => [
            ':input[name="group_formatter"]' => ['value' => 'layouts'],
          ],
        ],
      ];
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function getDisplaySettings() {
    $group = $this->group;
    if ($this->context === 'form') {
      $display = EntityFormDisplay::load("{$group->entity_type}.{$group->bundle}.{$group->mode}");
    }
    elseif ($this->context === 'view') {
      $display = EntityViewDisplay::load("{$group->entity_type}.{$group->bundle}.{$group->mode}");
    }
    return $display;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = parent::settingsSummary();

    $field_layout = $this->getSetting('field_layout') ?? $this->group?->field_layout ?? NULL;

    $layout = \Drupal::service('plugin.manager.core.layout')
      ->getDefinition($field_layout);

    $summary[] = $this->t('Layout: @layout', [
      '@layout' => $layout->getLabel() ?? FALSE
        ? (string) $layout->getLabel() . " ($field_layout)"
        : $field_layout
    ]);

    return $summary;
  }

}
